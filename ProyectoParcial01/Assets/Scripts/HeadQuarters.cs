﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadQuarters : MonoBehaviour {

    private int life = 10;
    public Texture2D textureToDisplay;
	void OnGUI()
    {
        GUI.Label(new Rect(50, 15, 100, 20), life.ToString());
        GUI.Label(new Rect(10, 10, textureToDisplay.width, textureToDisplay.height), textureToDisplay);
        
    }
	void Start () {
		
	}
	
	
	void Update () {
        if (life <= 0)
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
	}
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            life = life - 1;
            Destroy(other.gameObject, 2.0f);
        }
    }
}
