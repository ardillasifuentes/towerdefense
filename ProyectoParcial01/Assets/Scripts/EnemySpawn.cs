﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemySpawn : MonoBehaviour {

    public void StartWave()
    {
        StartCoroutine(WaveStart());
        
    }
    public enum EnemyTypes
    {
        buggy,
        helicopter,
        hover
    };
    [System.Serializable]
    public struct Waves
    {
        public EnemyTypes[] wave;
    }
    public Waves[] waves;
    [SerializeField]
    private GameObject buggyPrefab;
    [SerializeField]
    private GameObject helicopterPrefab;
    [SerializeField]
    private GameObject hoverPrefab;
    [SerializeField]
    private int currentWave = 0;
    [SerializeField]
    private int currentEnemy = 0;
    [SerializeField]
    private int timeBetweenWaves = 3;

    public IEnumerator WaveStart()
    {
        for(int i = 0; i < waves.Length; i++)
        {
            for ( int j = 0; j < waves[i].wave.Length; j++)
            {
                Instantiate(buggyPrefab, transform.position, Quaternion.identity);
                yield return new WaitForSeconds(timeBetweenWaves);
            }
            yield return new WaitForSeconds(timeBetweenWaves);
        }
    }
}
