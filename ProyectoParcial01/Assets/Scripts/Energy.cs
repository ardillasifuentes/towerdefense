﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour {
    public int energy = 10;
    public Texture2D textureToDisplay;

    void OnGUI()
    {
        GUI.Label(new Rect(50, 35, 100, 20), energy.ToString());
        GUI.Label(new Rect(10, 30, textureToDisplay.width, textureToDisplay.height), textureToDisplay);

    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SpawnEnergy()
    {
        StartCoroutine("EnergyCreator");
    }
    private IEnumerator EnergyCreator()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            energy = energy + 1;
        }
    }
}
