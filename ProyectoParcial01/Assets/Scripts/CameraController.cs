﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField] private Vector2 mousePos;
    [SerializeField] private float border = 5f;
    [SerializeField] private float cameraMoveSpeed = 20f;

    
	void Update () {
        MoveCamera();
	}
    void MoveCamera()
    {
        Vector3 CamPos = transform.position;
        if (Input.mousePosition.y >= Screen.height - border && CamPos.z < -30)
        {
            CamPos.z += cameraMoveSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.y <= border && CamPos.z > -35)
        {
            CamPos.z -= cameraMoveSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x >= Screen.width - border && CamPos.x < 5)
        {
            CamPos.x += cameraMoveSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x <=border && CamPos.x  > -10)
        {
            CamPos.x -= cameraMoveSpeed * Time.deltaTime;
        }
        transform.position = CamPos;

    }
}
