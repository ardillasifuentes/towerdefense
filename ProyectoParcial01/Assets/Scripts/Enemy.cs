﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    private NavMeshAgent agent;
    private GameObject target;
    private int life = 10;
    

	void Start () {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("HomeBaseTarget");
        agent.SetDestination(target.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		if (life <= 0)
        {
            Destroy(this.gameObject);
        }
	}
    void OnTriggerEnter(Collider other)
    {
        StartCoroutine("Die");
    }

    public IEnumerator Die()
    {
        while (true)
        {
            life = life - 1;
            yield return new WaitForSeconds(1);
        }
    }
   

}
