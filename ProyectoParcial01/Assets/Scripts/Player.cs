﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject machineGunPrefab;
    [SerializeField]
    private GameObject holdingWeapon;
    [SerializeField]
    private Energy energyLev;

    public void CreateWeapon(string weaponType)
    {
        if (holdingWeapon != null)
        {
            return;
        }
        if (energyLev.energy >= 4)
        {

            holdingWeapon = Instantiate(machineGunPrefab);
            energyLev.energy = energyLev.energy - 4;
            
        }
    }



   private void Update()
    {
        if (holdingWeapon != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                holdingWeapon.transform.position = hitInfo.point;
                if (Input.GetMouseButtonDown(0))
                {
                    if (hitInfo.transform.CompareTag("Placement"))
                    {
                        holdingWeapon = null;
                        Destroy(hitInfo.collider);
                    }
                }
            }
        }
    }
}